#import "CommonUtilPlugin.h"
#if __has_include(<common_util/common_util-Swift.h>)
#import <common_util/common_util-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "common_util-Swift.h"
#endif

@implementation CommonUtilPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCommonUtilPlugin registerWithRegistrar:registrar];
}
@end
