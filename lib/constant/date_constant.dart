const int dateDayOfDays = 1;
const int dateDayOfWeeks = 7;
const int dateDayOfMonths = 31;
const int dateDayOfYears = dateDayOfMonths * 12;

const int dateMilliseconds = 1000;
const int dateMicroseconds = 1000000;

const int dateSecondOfMinute = 60;
const int dateSecondOfHour = 3600;
const int dateSecondOfDay = 86400;
const int dateSecondOfWeek = dateSecondOfDay * dateDayOfWeeks;
const int dateSecondOfMonth = dateSecondOfDay * dateDayOfMonths;
const int dateSecondOfYear = dateSecondOfDay * dateDayOfYears;
const int dateSecondOfMinuteMillisecond = dateSecondOfMinute * dateMilliseconds;
const int dateSecondOfHourMillisecond = dateSecondOfHour * dateMilliseconds;
const int dateSecondOfDayMillisecond = dateSecondOfDay * dateMilliseconds;
const int dateSecondOfWeekMillisecond = dateSecondOfWeek * dateMilliseconds;
const int dateSecondOfMonthMillisecond = dateSecondOfMonth * dateMilliseconds;
const int dateSecondOfDayMicrosecond = dateSecondOfDay * dateMicroseconds;

const int dateSummarySecondJustBoundary = 1;
const int dateSummarySecondMinBoundary = dateSecondOfMinute;
const int dateSummarySecondHourBoundary = dateSecondOfHour;
const int dateSummarySecondDayBoundary = dateSecondOfDay;
const int dateSummarySecondWeekBoundary = dateSecondOfWeek;
const int dateSummarySecondMonthBoundary = dateSecondOfMonth;
const int dateSummarySecondDateBoundary = dateSecondOfMonth * 12;
const int dateSummarySecondYearBoundary = dateSummarySecondDateBoundary * 1000;