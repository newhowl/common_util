const double sizeIcon6 = 6.0;
const double sizeIcon10 = 10.0;
const double sizeIcon16 = 16.0;
const double sizeIcon20 = 20.0;
const double sizeIcon24 = 24.0;
const double sizeIcon32 = 32.0;
const double sizeIcon36 = 36.0;
const double sizeIcon44 = 44.0;
const double sizeIcon48 = 48.0;
const double sizeIcon60 = 60.0;
const double sizeIcon100 = 100.0;

const double sizePadding1 = 1.0;
const double sizePadding2 = 2.0;
const double sizePadding4 = 4.0;
const double sizePadding5 = 5.0;
const double sizePadding6 = 6.0;
const double sizePadding8 = 8.0;
const double sizePadding10 = 10.0;
const double sizePadding12 = 12.0;
const double sizePadding13 = 13.0;
const double sizePadding14 = 14.0;
const double sizePadding16 = 16.0;
const double sizePadding18 = 18.0;
const double sizePadding20 = 20.0;
const double sizePadding24 = 24.0;
const double sizePadding30 = 30.0;
const double sizePadding34 = 34.0;
const double sizePadding40 = 40.0;

const double sizeGap1 = 1.0;
const double sizeGap2 = 2.0;
const double sizeGap3 = 3.0;
const double sizeGap4 = 4.0;
const double sizeGap5 = 5.0;
const double sizeGap6 = 6.0;
const double sizeGap8 = 8.0;
const double sizeGap10 = 10.0;
const double sizeGap12 = 12.0;
const double sizeGap16 = 16.0;
const double sizeGap20 = 20.0;
const double sizeGap24 = 24.0;

const double sizeBorder0 = 0.0;
const double sizeBorder1 = 1.0;
const double sizeBorder2 = 2.0;
const double sizeBorder3 = 3.0;
const double sizeBorder4 = 4.0;
const double sizeBorder8 = 8.0;
const double sizeBorder16 = 16.0;
const double sizeBorder64 = 64.0;

const double sizeBorderWidth0_5 = 0.5;
const double sizeBorderWidth1_0 = 1.0;

const double sizeItemHeight36 = 36.0;
const double sizeItemHeight50 = 50.0;
const double sizeItemHeight56 = 56.0;