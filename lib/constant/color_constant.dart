import 'package:flutter/material.dart';

///
/// White
///
const Color colorWhite = Color(0xFFFFFFFF);

///
/// Black
///
const Color colorBlack = Color(0xFF000000);
const Color colorBlack333333 = Color(0xFF333333);

///
/// Grey
///
const Color colorGreyC7C7C7 = Color(0xFFC7C7C7);
const Color colorGreyBDBDBD = Color(0xFFBDBDBD);
const Color colorGrey404040 = Color(0xFF404040);

///
/// Black
///
const Color colorBlack202020 = Color(0xFF202020);


///
/// red
///
const Color colorRed = Color(0xFFFF0000);

///
/// Pink
///
const Color colorPinkFF31B1 = Color(0xFFFF31B1);

///
/// Blue
///
const Color colorBlue5A7BEF = Color(0xFF5A7BEF);
const Color colorBlueA1ACC0 = Color(0xFFA1ACC0);
const Color colorBlue2F80ED = Color(0xFF2F80ED);

///
/// Clear
///
const Color colorClear = Color(0x00000000);
const Color colorClearHalf = Color(0x88000000);
const Color colorClearDark = Color(0xBB000000);
const Color colorClearWhite = Color(0xCCFFFFFF);

///
/// Clear Black
///
const Color colorClearBlack003 = Color(0x03FFFFFF);
const Color colorClearBlack010 = Color(0x19000000);
const Color colorClearBlack020 = Color(0x33000000);
const Color colorClearBlack030 = Color(0x4D000000);
const Color colorClearBlack040 = Color(0x66000000);
const Color colorClearBlack050 = Color(0x80000000);
const Color colorClearBlack060 = Color(0x99000000);
const Color colorClearBlack070 = Color(0xB3000000);
const Color colorClearBlack080 = Color(0xCD000000);
const Color colorClearBlack090 = Color(0xE6000000);



