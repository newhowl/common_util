import 'dart:async';

typedef FutureOrVoidParamStringCallBack = FutureOr<void> Function(String str);
typedef VoidModifyParamCallback = void Function(dynamic item);