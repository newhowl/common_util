import 'package:shared_preferences/shared_preferences.dart';

class DevelopmentManager {
  static final _instance = DevelopmentManager._internal();

  DevelopmentManager._internal();

  factory DevelopmentManager() {
    return _instance;
  }

  ///
  ///
  ///
  static final String _kDevelopMode = 'development_manager_develop_mode';

  ///
  ///
  ///
  static Future<bool> getPreferenceDevelopMode() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_kDevelopMode) ?? false;
  }

  static Future<bool> setPreferenceDevelopMode(bool mode) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_kDevelopMode, mode);
  }

  ///
  ///
  ///
  bool _isDebugMode = false;
  bool get isDebugMode => _isDebugMode;
  set isDebugMode(value) => _isDebugMode = value;

  void judgmentDebugModeByAppPackageName({bool isDebugMode = false}) {
    this.isDebugMode = isDebugMode;
  }

  ///
  ///
  ///
  bool _isDevelopmentMode = true;
  bool get isDevelopmentMode => (_isDevelopmentMode && _isDebugMode);
  set isDevelopmentMode(value) => _isDevelopmentMode = value;

  bool isPossibleReport(int reportCount) {
    return (isDevelopmentMode) ? true : (0 == reportCount);
  }

  bool isPossibleOverlapLike() {
    return (isDevelopmentMode) ? true : false;
  }

  Future<void> loadDebugMode() async {
    await getPreferenceDevelopMode().then((value) {
      isDevelopmentMode = value;
    });
  }
}
