import 'api_result.dart';

///
/// when complete
///
typedef ApiResultSuccessCallback = Future<void> Function(dynamic fetch, dynamic data);
typedef ApiResultFailedCallback = Future<void> Function(ApiResultType type, String msg);
typedef ApiResultSuccessAtNullFetchCallback = Future<void> Function();

///
/// when get
///
typedef OnApiResultCacheGetCallback = Future<dynamic> Function();
typedef OnApiResultDBGetCallback= Future<dynamic> Function();
typedef OnApiResultGetApiCallback = Future<dynamic> Function();
typedef OnApiResultCacheUpdateCallback = Future<dynamic> Function();
typedef OnApiResultDBUpdateCallback = Future<dynamic> Function();

///
/// make
///
typedef MapToDynamicCallback = dynamic Function(Map mdp);