import 'package:common_util/logger/logger.dart';
import 'package:common_util/util/map_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:flutter/material.dart';

import 'api_result_typedef.dart';

enum ApiResultType {
  init,
  success,
  failed,
  failed_upload_file,
  failed_fetch_data,
  failed_not_available,
  timeout,
  exception,
  exception_server,
  exception_local,
  exception_function,
  null_argument,
  null_data,
  null_result,
  permission,
  blacklist,
  impossible_self,
  wrong_session,
  wrong_access_token,
  wrong_argument,
  wrong_valid_date,
}

class ApiResult {
  ApiResultType type = ApiResultType.init;
  dynamic fetch;  // map, list<map>
  dynamic data; // model, list<model>
  dynamic maps;
  var refMsg = '';

  void dispose() {
    fetch = null;
    data = null;
    maps = null;
  }

  int getCodeByType() {
    var code = 0;
    switch(type) {
      case ApiResultType.init: code = 0; break;
      case ApiResultType.success: code = 200; break;
      case ApiResultType.failed: code = 300; break;
      case ApiResultType.failed_upload_file: code = 301; break;
      case ApiResultType.failed_fetch_data: code = 302; break;
      case ApiResultType.failed_not_available: code = 303; break;
      case ApiResultType.timeout: code = 400; break;
      case ApiResultType.exception: code = 500; break;
      case ApiResultType.exception_server: code = 501; break;
      case ApiResultType.exception_local: code = 502; break;
      case ApiResultType.exception_function: code = 503; break;
      case ApiResultType.null_argument: code = 600; break;
      case ApiResultType.null_data: code = 601; break;
      case ApiResultType.null_result: code = 602; break;
      case ApiResultType.permission: code = 700; break;
      case ApiResultType.blacklist: code = 800; break;
      case ApiResultType.impossible_self: code = 900; break;
      case ApiResultType.wrong_session: code = 1000; break;
      case ApiResultType.wrong_access_token: code = 1001; break;
      case ApiResultType.wrong_argument: code = 1002; break;
      case ApiResultType.wrong_valid_date: code = 1003; break;
    }
    return code;
  }

  String getCodeStringByType() {
    var code = '';
    switch(type) {
      case ApiResultType.init: code = 'init'; break;
      case ApiResultType.success: code = 'success'; break;
      case ApiResultType.failed: code = 'failed'; break;
      case ApiResultType.failed_upload_file: code = 'failed_upload_file'; break;
      case ApiResultType.failed_fetch_data: code = 'failed_fetch_date'; break;
      case ApiResultType.failed_not_available: code = 'failed_not_available'; break;
      case ApiResultType.timeout: code = 'timeout'; break;
      case ApiResultType.exception: code = 'exception'; break;
      case ApiResultType.exception_server: code = 'exception_server'; break;
      case ApiResultType.exception_local: code = 'exception_local'; break;
      case ApiResultType.exception_function: code = 'exception_function'; break;
      case ApiResultType.null_argument: code = 'null_argument'; break;
      case ApiResultType.null_data: code = 'null_data'; break;
      case ApiResultType.null_result: code = 'null_result'; break;
      case ApiResultType.permission: code = 'permission'; break;
      case ApiResultType.blacklist: code = 'blacklist'; break;
      case ApiResultType.impossible_self: code = 'impossible_self'; break;
      case ApiResultType.wrong_session: code = 'wrong_session'; break;
      case ApiResultType.wrong_access_token: code = 'wrong_access_token'; break;
      case ApiResultType.wrong_argument: code = 'wrong_argument'; break;
      case ApiResultType.wrong_valid_date: code = 'wrong_valid_date'; break;
    }
    return code;
  }

  ApiResult setApiResult({
    ApiResultType? type, dynamic fetch, dynamic data, dynamic maps, String? msg,
  }) {
    if (isExists(type)) this.type = type!;
    if (isExists(fetch)) this.fetch = fetch;
    if (isExists(data)) this.data = data;
    if (isExists(maps)) this.maps = maps;
    if (isExists(msg)) refMsg = msg!;
    return this;
  }

  bool checkSuccess() {
    return (ApiResultType.success == type);
  }

  bool checkSuccessWithFetch() {
    return (ApiResultType.success == type && isNotExists(fetch));
  }

  bool checkInit() {
    return (ApiResultType.init == type);
  }

  Future<dynamic> makeDataByMap({MapToDynamicCallback? onMake}) async {
    var isCorrect = checkSuccess() && isExists(fetch) && (fetch is Map);
    if (isCorrect) {
      data = onMake?.call(fetch);
      return data;
    }
    return null;
  }

  void addResultMap(String key, dynamic value) {
    var isMap = fetch is Map;
    if (isMap) {
      setItemFromMap(fetch, key, value);
    }
  }

  Future<dynamic> whenGet({
    @required String? title,
    OnApiResultCacheGetCallback? onGetCache,
    OnApiResultDBGetCallback? onGetDB,
    OnApiResultGetApiCallback? onGetApi,
    OnApiResultCacheUpdateCallback? onUpdateCache,
    OnApiResultDBUpdateCallback? onUpdateDB,
    bool priorityCache = false,
  }) async {
    if (priorityCache) {
      data = await onGetCache?.call();
      if (isExists(data)) {
        setApiResult(type: ApiResultType.success);
        return data;
      }
    }
    maps = await onGetDB?.call();
    if (isExists(maps)) {
      data = await onUpdateCache?.call();
      setApiResult(type: ApiResultType.success);
    }
    if (isExists(data)) {
      onGetApi?.call().then((value) async {
        fetch = value;
        if (isExists(fetch)) {
          if (isExists(onUpdateDB)) {
            onUpdateDB?.call();
          }
          if (isExists(onUpdateCache)) {
            data = await onUpdateCache?.call();
          }
        }
      });
    } else {
      fetch = await onGetApi?.call();
      await onGetApi?.call();
      if (isExists(fetch)) {
        if (isExists(onUpdateDB)) {
          onUpdateDB?.call();
        }
        if (isExists(onUpdateCache)) {
          data = await onUpdateCache?.call();
        }
      }
    }
    return data;
  }

  Future<void> whenComplete({
    ApiResultSuccessCallback? onSuccess,
    ApiResultFailedCallback? onFailed,
    ApiResultSuccessAtNullFetchCallback? onSuccessAtNullFetch,
  }) async {
    var isSuccess = checkSuccess();
    if (isSuccess) {
      if (isExists(onSuccessAtNullFetch) && isNotExists(fetch) && isNotExists(data)) {
        await onSuccessAtNullFetch?.call();
      } else {
        await onSuccess?.call(fetch, data);
      }
    } else {
      var errMsg = getCodeStringByType() + ' ' +  refMsg;
      await onFailed?.call(type, errMsg);
    }
  }

  Future<void> whenCompleteWithNull({
    ApiResultSuccessCallback? onSuccess,
    ApiResultFailedCallback? onFailed,
    ApiResultSuccessAtNullFetchCallback? onSuccessAtNullFetch,
  }) async {
    var isSuccess = checkSuccess();
    if (isSuccess) {
      if (isExists(fetch) || isExists(data)) {
        await onSuccess?.call(fetch, data);
      } else {
        await onSuccessAtNullFetch?.call();
      }
    } else {
      var errMsg = getCodeStringByType() + ' ' +  refMsg;
      await onFailed?.call(type, errMsg);
    }
  }

  @override
  String toString() {
    return 'toString > User > '
        'type: $type '
        'fetch: $fetch '
        'data: $data '
        'refMsg: $refMsg '
    ;
  }
}