import 'package:common_util/development/development_manager.dart';
import 'package:logger/logger.dart';

//v : verbose	Show detail of logging than the usual one
//d : debug	Use when log for debugging purpose
//i : info	Log info
//w : warning	Log warning messages
//e : error	Log error messages
//w : wtf	You know it right?

class DevLogFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    if (!DevelopmentManager().isDevelopmentMode) {
      return false;
    }

    var shouldLog = false;
    assert(() {
      if (event.level.index >= Logger.level.index) {
        shouldLog = true;
      }
      return true;
    }());
    return shouldLog;
  }
}

var logHelper = Logger(
    filter: DevLogFilter(),
    printer: PrettyPrinter(
      colors: true,
      errorMethodCount: 1,
      printEmojis: true,
      printTime: true,
      lineLength: 80,
      methodCount: 0,
    ));