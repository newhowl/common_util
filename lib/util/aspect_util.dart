import 'int_util.dart';

double aspectBySize(double width, double height) {
  if (isNotPositiveDouble(width)) return 1.0;
  if (isNotPositiveDouble(height)) return 1.0;

  return width / height;
}

double aspectHeightByWidth(double width, double aspect) {
  if (isNotPositiveDouble(width)) return 0.0;
  if (isNotPositiveDouble(aspect)) return 0.0;

  return width / aspect;
}