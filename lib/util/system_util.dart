import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/*
 * StatusBar, NavigationBar 색상 설정.
 */
void setSystemStatusBarStyle({Color navigationBarColor = Colors.white}) {
  var statusBarColor = Colors.white.withOpacity(0.0);

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: statusBarColor,
    statusBarBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarColor: navigationBarColor,
    systemNavigationBarIconBrightness: Brightness.dark,
  ));
}

/*
 * 가로/세로 모드를 설정.
 */
void setSystemPreferredOrientations() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}
