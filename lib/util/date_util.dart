import 'package:common_util/util/int_util.dart';
import 'package:common_util/util/object_util.dart';

int dateMillisecond() {
  return DateTime.now().millisecondsSinceEpoch;
}

int dateTimeNow() {
  return dateMillisecond();
}

bool checkValidDateTimeNow(int dateTime, int validTime) {
  if (isNotExists(dateTime)) return false;
  if (isNotExists(validTime)) return false;
  if (isNotPositiveNum(dateTime)) return false;
  if (isNotPositiveNum(validTime)) return false;

  var diff = dateTimeNow() - dateTime;
  var diffAbs = diff.abs();
  return (0 < validTime - diffAbs);
}




