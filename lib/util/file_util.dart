import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:path_provider/path_provider.dart';

import 'object_util.dart';

const double fileImageCropWidthMax = 2048.0;
const double fileImageCropHeightMax = 2048.0;

Future<File> writeFileByByteData(String fileName, ByteData byteData) async {
  var dir = (await getApplicationDocumentsDirectory()).path + fileName;
  var byteBuffer = byteData.buffer;
  return await File(dir).writeAsBytes(byteBuffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
}

Future<File?> resizeFileImage(BuildContext context, File croppedFile, {
  double maxWidth = fileImageCropWidthMax,
  double maxHeight = fileImageCropHeightMax,
}) async {
  if (isNotExists(croppedFile)) {
    return null;
  }
  var maxSize = Size(maxWidth, maxHeight);
  var resizeSize = await getImageSizeDownScaleAndRatio(croppedFile, maxSize.width, maxSize.height);
  var resizeFile = await FlutterNativeImage.compressImage(croppedFile.path,
      targetWidth: resizeSize.width.toInt(), targetHeight: resizeSize.height.toInt());

  return resizeFile;
}

Future<Size> getImageSizeDownScaleAndRatio(
    File file,
    double maxWidth,
    double maxHeight,
    ) async {
  if (0.0 >= maxWidth || 0.0 >= maxHeight) {
    return Size(0.0, 0.0);
  }
  Size result;
  var size = await getImageSizeOfFile(file);
  var isDownScale = (size.width > maxWidth - 10.0 || size.height > maxHeight - 10.0);
  var isWidthMode = (size.width > size.height);
  if (isDownScale) {
    if (isWidthMode) {
      var ratio = size.height / size.width;
      var width = maxWidth;
      var height = width * ratio;
      result = Size(width, height);
    } else {
      var ratio = size.width / size.height;
      var height = maxHeight;
      var width = height * ratio;
      result = Size(width, height);
    }
  } else {
    result = Size(size.width, size.height);
  }
  return result;
}

Future<Size> getImageSizeOfFile(File file) async {
  if (isNotExists(file)) {
    return Size(0.0, 0.0);
  }
  var decodedImage = await decodeImageFromList(file.readAsBytesSync());
  var width = (isExists(decodedImage)) ? decodedImage.width : 0;
  var height = (isExists(decodedImage)) ? decodedImage.height : 0;
  return Size(width.toDouble(), height.toDouble());
}