import 'package:flutter/material.dart';

double widthMedia(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double heightMedia(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

double heightMediaTopWithAppbar(BuildContext context) {
  return MediaQuery.of(context).padding.top + kToolbarHeight;
}