import 'package:flutter/material.dart';

Color colorOpacity(Color color, double opacity) {
  return color.withOpacity(opacity);
}