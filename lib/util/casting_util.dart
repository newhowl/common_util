T? asType<T>(dynamic data) {
  var isAs = data is T;
  var model = isAs ? data as T : null;
  return model;
}