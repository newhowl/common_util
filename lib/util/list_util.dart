import 'int_util.dart';
import 'object_util.dart';

T? getItemFromList<T>(List<dynamic>? lists, int index) {
  if (isNotExists(lists) || isNotExists(index)) {
    return null;
  }
  var length = lengthByList(lists);
  var isCorrect = 0 <= index && index < length;
  return isCorrect ? lists![index] : null;
}

int lengthByList(List? list) {
  return list?.length ?? 0;
}

bool isExistsList(List? list) {
  return isPositiveNum(list?.length ?? 0);
}

String transStringByList(List<String>? listStr) {
  var result = '';
  if (isExists(listStr)) {
    var length = listStr?.length ?? 0;
    for (var index = 0; index < length; index++) {
      result += listStr![index];
      if (index < length - 1) {
        result += ',';
      }
    }
  }
  return result;
}