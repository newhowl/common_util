import 'package:common_util/logger/logger.dart';
import 'package:common_util/util/string_util.dart';

import 'object_util.dart';

String getLinkByUrl(String url) {
  if (isNotExistsStr(url)) return '';

  var isNotMatch = true;
  var result = url;
  for (var exp in [
    RegExp(r'^http:\/\/'),
    RegExp(r'^https:\/\/'),
  ]) {
    var match = exp.firstMatch(url);
    // Match match = exp.firstMatch(url) as Match;
    if (isExists(match)) {
      isNotMatch = false;
    }
  }
  if (isNotMatch) {
    result = 'https://' + url;
  }

  return result;
}

bool matchUrlLink(String link, String? url) {
  if (isNotExists(link)) return false;
  if (isNotExists(url)) return false;

  var hasMatch = RegExp(link).hasMatch(url!);
  return hasMatch;
}