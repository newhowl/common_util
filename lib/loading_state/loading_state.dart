import 'package:common_util/api_result/api_result.dart';
import 'package:common_util/logger/logger.dart';
import 'package:common_util/util/date_util.dart';
import 'package:common_util/util/int_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:flutter/material.dart';

import 'loading_state_typedef.dart';

class LoadingState {
  bool isLoaded = false;
  bool noMore = false;
  int loadedYmdt = 0;
  LoadingStateType loadingStateType = LoadingStateType.INIT;

  LoadingState({
    this.loadingStateType = LoadingStateType.INIT,
    this.isLoaded = false,
  });

  LoadingState.init({
    this.loadingStateType = LoadingStateType.INIT,
    this.isLoaded = false,
  });

  LoadingState.done({
    this.loadingStateType = LoadingStateType.DONE,
    this.isLoaded = false,
  }) {
    loadedYmdt = dateTimeNow();
  }

  void init() {
    loadingStateType = LoadingStateType.INIT;
    isLoaded = false;
  }

  void start() {
    loadingStateType = LoadingStateType.LOADING;
  }

  void loading() {
    loadingStateType = LoadingStateType.LOADING;
  }

  void end({LoadingStateType type = LoadingStateType.DONE}) {
    isLoaded = true;
    loadingStateType = type;
    loadedYmdt = dateTimeNow();
  }

  void error({bool force = false}) {
    isLoaded = true;
    if (LoadingStateType.LOADING == loadingStateType || force) {
      loadingStateType = LoadingStateType.ERROR;
    }
  }

  Future<bool> doProcess({
    required String title,
    bool onlyOnce = false,
    int? validTime,
    VoidCallback? onLoading,
    FutureBoolCallback? onPrefix,
    FutureCallback? onProcess, // fetch 동작
    FutureCallback? onProcessed, // process 가 종료된 후 (화면 갱신 등)
    FutureBoolCallback? onSetResult, // 결과에 따라 LoadingState를 end/error로 변경한다.
    FutureCallback? onUpdate,  // fetch 후 획득한 정보 업데이트
    FutureCatchCallback? onCatch, // exception 발생한 경우
    VoidCallback? onFinally, //
  }) async {
    if (checkLoading()) {
      onLoading?.call();
      return false;
    }
    if (onlyOnce && checkLoaded()) return false;
    if (isPositiveNum(loadedYmdt) && isExists(validTime) && checkValidDateTimeNow(loadedYmdt, validTime!)) {
      return false;
    }

    try {
      start();
      var correct = await onPrefix?.call() ?? true;
      if (!correct) {
        end();
        return true;
      }

      await onProcess?.call();
      await onProcessed?.call();
      if (null != onSetResult) {
        var isResult = await onSetResult.call();
        if (isTrue(isResult)) {
          end();
        } else {
          error(force: true);
        }
      } else {
        end();
      }
      await onUpdate?.call();
      return true;
    } catch(e) {
      end();
      logHelper.e('doProcess > $title > $e');
      await onCatch?.call(e);
      // FirebaseCrashlytics.instance.log(e.toString());
      return false;
    } finally {
      onFinally?.call();
    }
  }

  Future<bool> doProcessWithApiResult({
    BuildContext? context,
    required String title,
    bool onlyOnce = false,
    int? validTime,
    LoadingStatePrefixCallback? onPrefix,
    LoadingStateProcessCallback? onProcess, // fetch 동작
    LoadingStateProcessedCallback? onProcessed, // process 가 종료된 후 (화면 갱신 등)
    LoadingStateSuccessCallback? onSuccess,
    LoadingStateSuccessAtNullFetchCallback? onSuccessAtNullFetch,
    LoadingStateFailedCallback? onFailed,
    LoadingStateCatchCallback? onCatch, // exception 발생한 경우
    LoadingStateFinallyCallback? onFinally, //
    bool isRefresh = false,
  }) async {
    if (checkLoading()) {
      return false;
    }
    if (onlyOnce && checkLoaded()) return false;
    if (isPositiveNum(loadedYmdt) && isExists(validTime) && checkValidDateTimeNow(loadedYmdt, validTime!)) {
      return false;
    }

    var apiResult = ApiResult();
    loadedYmdt = dateTimeNow();

    try {
      start();
      var correct = await onPrefix?.call() ?? true;
      if (!correct) {
        end();
        return correct;
      }

      await onProcess?.call(apiResult, isRefresh: isRefresh);
      if (isRefresh) {
        await onProcessed?.call(apiResult, isRefresh: isRefresh);
      }
      if (isTrue(apiResult.checkSuccess())) {
        end();
      } else {
        error(force: true);
      }
      await apiResult.whenCompleteWithNull(
        onSuccess: (fetch, data) async {
          await onSuccess?.call(apiResult, isRefresh: isRefresh);
        },
        onSuccessAtNullFetch: () async {
          await onSuccessAtNullFetch?.call(apiResult, isRefresh: isRefresh);
        },
        onFailed: (type, msg) async {
          logHelper.w('doProcessWithApiResult > failed($title) > $title(${apiResult.type}/${apiResult.refMsg})');
          debugPrint('doProcessWithApiResult > failed($title) > $title(${apiResult.type}/${apiResult.refMsg})');
          await onFailed?.call(apiResult, isRefresh: isRefresh);
        },
      );
      return true;
    } catch(e) {
      end();
      logHelper.e('doProcessWithApiResult > catch($title) > $title(${apiResult.type}) > $e');
      debugPrint('doProcessWithApiResult > catch($title) > $title(${apiResult.type}) > $e');
      await onCatch?.call(e);
      // FirebaseCrashlytics.instance.log(e.toString());
      return false;
    } finally {
      await onFinally?.call(apiResult, isRefresh: isRefresh);
      apiResult.dispose();
    }
  }

  @override
  String toString() {
    return 'Channel > '
        'isLoaded : ' + isLoaded.toString()
    ;
  }

  bool checkLoading() {
    return (LoadingStateType.LOADING == loadingStateType);
  }

  bool checkDone() {
    return (LoadingStateType.DONE == loadingStateType);
  }

  bool checkInit() {
    return (LoadingStateType.INIT == loadingStateType);
  }

  bool checkError() {
    return (LoadingStateType.ERROR == loadingStateType);
  }

  bool checkLoaded() {
    return isLoaded;
  }

  bool checkValidDate(int validTime) {
    if (isNotExists(validTime)) return false;

    return checkValidDateTimeNow(loadedYmdt, validTime);
  }
}

enum LoadingStateType { INIT, DONE, LOADING, WAITING, ERROR, PLAY }
